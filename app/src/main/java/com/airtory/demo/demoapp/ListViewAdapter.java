package com.airtory.demo.demoapp;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ashish on 2/20/2017.
 */
public class ListViewAdapter extends BaseAdapter {

    // Declare Variables

    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;

    HashMap<String, String> resultp = new HashMap<String, String>();

    public ListViewAdapter(Context context,
                           ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView name;
        TextView desc;
        RelativeLayout relativeLayout;
        ImageView icon;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.list_item, parent, false);
        // Get the position
        resultp = data.get(position);

        // Locate the TextViews in listview_item.xml
        name = (TextView) itemView.findViewById(R.id.formatname);
        desc = (TextView) itemView.findViewById(R.id.formatdesc);
        icon = (ImageView) itemView.findViewById(R.id.formaticon);
        relativeLayout=(RelativeLayout)itemView.findViewById(R.id.listitem);

        // Capture position and set results to the TextViews
        name.setText(resultp.get("name"));
        desc.setText(resultp.get("desc"));

    //    Toast.makeText(context,resultp.get("number"),Toast.LENGTH_SHORT).show();
        int id = context.getResources().getIdentifier("com.airtory.demo.demoapp:drawable/" + resultp.get("image"), null, null);
        icon.setImageResource(id);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AdActivity.class);
                resultp = data.get(position);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                 i.putExtra(AdActivity.FORMATNUMBER,resultp.get("number"));
                  context.startActivity(i);
            }
        });
        return itemView;
    }


}
