package com.airtory.demo.demoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class listActivity extends AppCompatActivity {
    String jsonStore;
    ListViewAdapter adapter;
    private ListView lv;
    ArrayList<HashMap<String, String>> formatList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        formatList = new ArrayList<>();
        lv = (ListView) findViewById(R.id.list);

        jsonStore="[{'name':'Scratch To Reveal','image':'scratch','desc':'Scratch to reveal the offer. Click anywhere to redirect the page','number':'53'},{'name':'Peel To Reveal','image':'peelicon','desc':'Peel to reveal, we have hidden something interesting beneath.','number':'52'},{'name':'Shake To Reveal','image':'shakeicon','desc':'This format features shake functionality of your device to display the ad','number':'51'},{'name':'Scratch To Win','image':'scratchtowin','desc':'Featuring a scratchcard, which can be scratched to win exciting offers.','number':'57'},{'name':'Image Flow','image':'imageflow','desc':'Tilt the device to pan and see the scenery. Ideal for Travel and Tourism campaigns.','number':'54'},{'name':'Fade In','image':'fadein','desc':'Featuring an ad format for a presentation style display of your product','number':'55'},{'name':'Slide','image':'slider','desc':'Add images and videos as slides','number':'56'},{'name':'Photosphere','image':'photosphere','desc':'This gives you a 360 view experience','number':'45'}]";
        try {
            JSONArray jsonArray = new JSONArray(jsonStore);
            for(int i=0;i<jsonArray.length();i++) {
                JSONObject f=jsonArray.getJSONObject(i);
                String name=f.getString("name");
                String image=f.getString("image");
                String desc=f.getString("desc");
                String number=f.getString("number");

                HashMap<String,String> format=new HashMap<>();
                format.put("name",name);
                format.put("image",image);
                format.put("desc",desc);
                format.put("number",number);
                formatList.add(format);
            }
        }catch (Exception e) {

        }
        adapter = new ListViewAdapter(listActivity.this, formatList);
        lv.setAdapter(adapter);

    }


}
