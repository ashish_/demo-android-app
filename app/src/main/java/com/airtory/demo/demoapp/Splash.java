package com.airtory.demo.demoapp;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class Splash extends AppCompatActivity {
    TextView t1;
    ImageView myImage;

    ImageView backImage;
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        t1 = (TextView) findViewById(R.id.company_name);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Dosis-Medium.ttf");
        t1.setTypeface(face);
        backImage=(ImageView)findViewById(R.id.backImage);
        //cloud animation
        Animation animation = new TranslateAnimation(0, 0, 0, -20);
        animation.setDuration(1300);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setFillAfter(true);
        myImage = (ImageView) findViewById(R.id.baloon);

        myImage.startAnimation(animation);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        //cloud1
        ObjectAnimator cloudAnim1 = ObjectAnimator.ofFloat(findViewById(R.id.cloud1), "x", 50 , (width/2 +150));
        animator(cloudAnim1, 10000);
        //cloud2
        ObjectAnimator cloudAnim2 = ObjectAnimator.ofFloat(findViewById(R.id.cloud2), "x", width -50, -(width - 150));
        animator(cloudAnim2, 15000);
        //back image
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int backHeight = displaymetrics.heightPixels;
        int backWidth = displaymetrics.widthPixels;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(Splash.this,listActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);


    }
    public void animator(ObjectAnimator ele, int dur) {
        ele.setDuration(dur);
        ele.setRepeatCount(ValueAnimator.INFINITE);
        ele.setRepeatMode(ValueAnimator.RESTART);
        ele.start();
    }
}
