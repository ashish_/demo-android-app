var data;
$(document).ready(function(){

			var url = decodeURI(window.location.href);
            
			data = getParameterByName("data", url);
			data = JSON.parse(data);
			if(data == null){
				$.getJSON('data.json',function(raw){
					data = raw.data[0];
					update(data);
				});
			}else{
				update(data);
			}

		});
   
function convertHex(hex,opacity){
   hex = hex.replace('#','');
   r = parseInt(hex.substring(0,2), 16);
   g = parseInt(hex.substring(2,4), 16);
   b = parseInt(hex.substring(4,6), 16);

   result = 'rgba('+r+','+g+','+b+','+opacity+')';
   return result;
} 
		
	function update(data){
        
        var style = document.createElement('style');
        style.innerHTML = '.main_container{background-color:'+convertHex(data.backgroundColor,data.bgOpacity)+';}@media screen and (orientation: portrait){#overlay{background-image:url("' + data.overlay + '");}}@media screen and (orientation: landscape){#overlay{background-image: url("' + data.overlay_ls + '");background-position: initial;}}';
        style.type = "text/css";
        document.head.appendChild(style);
        
		var img2 = '<img src="'+data.instruct+'" id="instruct">';
		$(".container").append('<div id="overlay"></div>');
		$(".container").append(img2);
        $(document).ready(function(){
            console.log(data);
			$('#overlay').click(function(){
				window.open(data.url,'_blank');  
			});
		});

		var photoTilt = new PhotoTilt({
								url: data.background,
            					lowResUrl: data.background,
                                container: document.getElementById('tiltbody'),
            					maxTilt: 8
    					});
		
//				$(window).on("orientationchange",function(landscape){
//			var photoTilt = new PhotoTilt({
//								url: data.background,
//            					lowResUrl: data.background,
//                                container: document.getElementById('tiltbody'),
//            					maxTilt: 3
//    					});
//		});
        
        if (data.socialBlock === true || data.socialBlock === "true"){                 $('body').append('<div class="enable '+data.socialPos+'"><div class="fb-share-button fb-custom" data-href="'+data.fb+'" data-layout="button"></div><a href="https://twitter.com/intent/tweet?text='+data.tweet+'" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"/></div>');             }
	}
	
function getParameterByName(name, url) {
		if (!url) url = window.location.href;
			if(name == undefined){
				name = null;
				return name;
			}
                                    unescape(url);
			results=url.split('?'+name+'=');
			if (!results) return null;
			if (!results[1]) return '';
			return unescape(results[1]);
	}

	function getNameFromPath(strFilepath) {
		var objRE = new RegExp(/([^\/\\]+)$/);
    	var strName = objRE.exec(strFilepath);
 
		if (strName == null) {
			return null;
		}
		else {
			return strName[0];
		}
	}