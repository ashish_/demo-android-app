var data;
$(window).ready(function() {
            var url = decodeURI(window.location.href);
            data = getParameterByName("data", url);
            data = JSON.parse(data);
    if (data === null) {
        $.getJSON('data.json', function(raw) {
            data = raw.data[0];
            console.log(data);
            update(data);
        });
    } else {
        update(data);
    }
});

function convertHex(hex,opacity){
   hex = hex.replace('#','');
   r = parseInt(hex.substring(0,2), 16);
   g = parseInt(hex.substring(2,4), 16);
   b = parseInt(hex.substring(4,6), 16);

   result = 'rgba('+r+','+g+','+b+','+opacity+')';
   return result;
}

function update(data) {
    $(document).ready(function() {

        var style = document.createElement('style');
        style.innerHTML = '.main_container{background-color:'+convertHex(data.backgroundColor,data.bgOpacity)+';}@media screen and (orientation: portrait){#overlay{background-image:url("' + data.overlay + '");}#background{background-image: url("' + data.background + '");}}@media screen and (orientation: landscape){#overlay{background-image: url("' + data.overlay_landscape + '");}#background{background-image: url("' + data.background_landscape + '");}}';
        style.type = "text/css";
        document.head.appendChild(style);

        $('body').append('<div class="main_container"><div class="container"><div id="magazine"></div></div></div>');
        $('#magazine').append('<div id="overlay" class="resize"></div><div id="background" class="resize"></div>');

        $('#magazine').turn({
            display: 'single',
            autoCenter: true,
            acceleration: true,
            turnCorners: "bl,br",
            elevation: 100,
            page: 1,
        });

        window.addEventListener('resize', resize);

            function getSize() {
                        console.log('get size');
                        var width = document.body.clientWidth;
                        var height = document.documentElement.clientHeight;
                        return {
                            width: width,
                            height: height
                        }
            }

        function resize() {
            console.log('resize event triggered');

            var size = getSize();
            console.log(size);

            if(size.width>size.height){
                if(size.width>=1024){
                    size.width = 780;
                    size.height = 520;
                }else{
                    size.width =480;
                    size.height = 320;
                }
            }else{
                if(size.height>=1024){
                    size.height = 780;
                    size.width = 520;
                }else{
                    size.height =480;
                    size.width = 320;
                }
            }

            $('#magazine').turn('size', size.width, size.height);
            $('#magazine').turn("options", {turnCorners: "bl,br"});
            $("#magazine").turn("peel", "br");
        }

            $("#magazine").turn("peel", "br");
            $('#background').click(function(){
                        window.open(data.url,"_blank");
            });	
    });
    if (data.socialBlock === true || data.socialBlock === "true") {
        $('body').append('<div class="enable ' + data.socialPos + '"><div class="fb-share-button fb-custom" data-href="' + data.fb + '" data-layout="button"></div><a href="https://twitter.com/intent/tweet?text=' + data.tweet + '" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"/></div>');
    }


    $(window).bind('keydown', function(e) {

        if (e.keyCode == 37)
            $('#magazine').turn('previous');
        else if (e.keyCode == 39)
            $('#magazine').turn('next');
    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function redirectAd() {
    /*if(!flag['cta']){
            track("cta",creativeId);
            flag['cta'] = true;
				} */
    window.open(data.url, '_blank');
}