var data;
$(document).ready(function(){
    var url = decodeURI(window.location.href);
    data = getParameterByName("data", url);
    data = JSON.parse(data);
    if(data === null){
        $.getJSON('data.json',function(data){
            data = data.data[0];
            console.log(data);
            update(data);
        });         
    }
    else{
        update(data);
    }
});
var flag = false;
function update(data){
    
    var style = document.createElement('style');
        style.innerHTML = '@media screen and (orientation: portrait){.main_container{background-image:url("' + data.p_background + '");}}@media screen and (orientation: landscape){.main_container{background-image:url("' + data.ls_background + '");}}';
        style.type = "text/css";
        document.head.appendChild(style);
    
    $('body').append('<div class="main_container"><div class="container" ><img src="'+data.header+'" id="header"></img><div class="ls_container"><div id="logo"><img src="'+data.logo+'" id="logo_img"></img></div><div class="promo">'+data.promo+'</div><div class="promo2">'+data.promo2+'</div><div class="offer">'+data.offer+'</div><a href="'+data.url+'" target="_blank"><div class="cta" style="background-color:'+data.ctaColor+';color:'+data.ctaTextColor+'">'+data.cta+'</div></a></div></div></div>');
    $(document).ready(function(){
        $('.promo2').css("visibility",'hidden');
    });
    
    var mql = window.matchMedia("(orientation: portrait)");
	// If there are matches, we're in portrait
	function resize(mql){
		mql = window.matchMedia("(orientation: portrait)");
		console.log(mql);
		if($('#card')){
		   $('#card').remove();
		   }
        
        var canvas=document.createElement('div');
        canvas.setAttribute('id','card');
        canvas.setAttribute('class','scratchcard');
        canvas.innerHTML = '<canvas class="scratch" id="scratchpad_canvas" height="70px" width="290px">';
        var offr = document.getElementsByClassName('offer')[0];
        document.getElementsByClassName('ls_container')[0].insertBefore(canvas,offr);
//        document.getElementsByClassName('container')[0].insertBefore(canvas,offr);
//		$('.main_container').append('<div id="card" class="scratchpad" ><canvas class="canvas" id="scratchpad_canvas" ></div>');
		if(mql.matches) {  
            
            var header_img = document.getElementById('header');
            header_img.setAttribute('src',data.header);
            if (flag){
                return;
            }
			// Portrait orientation
			airScratch.init({
				percentage:30,
				containerid:"card",
				canvasid:"scratchpad_canvas",
				front:data.scratchcard,
                status:function(a){
                    console.log(a+"%");
                },
				completed:function(){
                    //$('.scratch').css("visibility",'hidden');
                    $('.promo').css("visibility",'hidden');
                    $('.promo2').css("visibility",'visible');
                    $('.promo2').css({"opacity":"1"});
                    //var style = document.createElement('style');
                    flag = true;
                    $('.cta').css({"opacity":"1","background-color":data.ctaColor,"color":data.ctaTextColor});
                }
			});
			
		} else {  
            
            var header_img = document.getElementById('header');
            header_img.setAttribute('src',data.landscape_header);
            if (flag){
                return;
            }
			// Landscape orientation
			airScratch.init({
				percentage:30,
				containerid:"card",
				canvasid:"scratchpad_canvas",
				front:data.scratchcard,
                status:function(a){
                    console.log(a+"%");
                },
				completed:function(){
                    //$('.scratch').css("visibility",'hidden');
                    $('.promo').css("visibility",'hidden');
                    $('.promo2').css("visibility",'visible');
                    $('.promo2').css({"opacity":"1"});
                    $('.cta').css({"opacity":"1","background-color":data.ctaColor,"color":data.ctaTextColor});
                    //var style = document.createElement('style');
                    flag = true;
                   
                }
			});
		}
	}
	mql.addListener(resize);
	resize(mql);
    
    if (data.socialBlock === true || data.socialBlock === "true"){
        $('body').append('<div class="enable '+data.socialPos+'"><div class="fb-share-button fb-custom" data-href="'+data.fb+'" data-layout="button"></div><a href="https://twitter.com/intent/tweet?text='+data.tweet+'" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"/></div>');
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    if(name === undefined){
        name = null;
        return name;
    }
    unescape(url);
    results=url.split('?'+name+'=');
    if (!results) return null;
    if (!results[1]) return '';
    return unescape(results[1]);
}

 