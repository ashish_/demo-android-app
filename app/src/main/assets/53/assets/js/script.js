var data;
$(document).ready(function(){
	$('#scratch-percent').hide();
	var url = decodeURI(window.location.href);
	data = getParameterByName("data", url);
	data = JSON.parse(data);
	if(data == null){
		$.getJSON('data.json',function(raw){
			data = raw.data[0];
			update(data);
		});
	}else{
		update(data);
	}
});

function convertHex(hex,opacity){
   hex = hex.replace('#','');
   r = parseInt(hex.substring(0,2), 16);
   g = parseInt(hex.substring(2,4), 16);
   b = parseInt(hex.substring(4,6), 16);

   result = 'rgba('+r+','+g+','+b+','+opacity+')';
   return result;
}

function update(data){
    
	var style = document.createElement('style');
	style.innerHTML = '.main_container{background-color:'+convertHex(data.backgroundColor,data.bgOpacity)+';}';
	style.type = "text/css";
	document.head.appendChild(style);
	        
	var mql = window.matchMedia("(orientation: portrait)");
	// If there are matches, we're in portrait
	function resize(mql){
		mql = window.matchMedia("(orientation: portrait)");
		console.log(mql);
		if($('#scratch')){
		   $('#scratch').remove();
		   }
		$('.content-box').append('<div id="scratch" class="scratchpad" ><canvas class="canvas" id="scratchpad_canvas" ></div>');
		if(mql.matches) {  
			// Portrait orientation
			airScratch.init({
				percentage:30,
				containerid:"scratch",
				canvasid:"scratchpad_canvas",
				front:data.front,
				back:data.back,
				completed:function(){
				  redirect();
				}
			})
			
		} else {  
			// Landscape orientation
			airScratch.init({
				percentage:30,
				containerid:"scratch",
				canvasid:"scratchpad_canvas",
				front:data.front_ls,
				back:data.back_ls,
				completed:function(){
				  redirect();
				}
			})
		}
	}
	mql.addListener(resize);
	resize(mql);

	$('#scratch').click(function(){
		window.open(data.url,"_blank");
	});	
	
	if (data.socialBlock === true || data.socialBlock === "true"){
		$('body').append('<div class="enable '+data.socialPos+'"><div class="fb-share-button fb-custom" data-href="'+data.fb+'" data-layout="button"></div><a href="https://twitter.com/intent/tweet?text='+data.tweet+'" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"/></div>');
	}
}
function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
	results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}