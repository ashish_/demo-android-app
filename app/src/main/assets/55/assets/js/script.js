var data;
$(document).ready(function(){
            var url = decodeURI(window.location.href);
            data = getParameterByName("data", url);
            data = JSON.parse(data);
           	if(data == null){
                        $.getJSON('data.json',function(data){
                                    data = data.data[0];
                                    console.log(data);
			update(data);
		});
            }
            else{
                        update(data);
            }
});

function convertHex(hex,opacity){
   hex = hex.replace('#','');
   r = parseInt(hex.substring(0,2), 16);
   g = parseInt(hex.substring(2,4), 16);
   b = parseInt(hex.substring(4,6), 16);

   result = 'rgba('+r+','+g+','+b+','+opacity+')';
   return result;
}

function update(data){
            
    var style = document.createElement('style');
    style.innerHTML = '.main-container{background-color:'+convertHex(data.backgroundColor,data.bgOpacity)+';}.container{background-color:'+data.backgroundColorContainer+';}.ctaBtn{background-color:'+data.backgroundColorCta+';color:'+data.textColorCta+';}.text{color:'+data.textColor+';}';
    style.type = "text/css";
    document.head.appendChild(style);
    
    //var style1 = document.createElement('style');
    //style1.innerHTML = '.container{background-color:'+convertHexcolor(data.backgroundColorContainer)+';}';
    //style1.type = "text/css";
    //document.head.appendChild(style1); 
            
    $('body').append('<div class="main-container"><div class="container"><div id="header_image"><img src="'+data.logo+'" align="middle"/></div><div class="text"><p id="txt1">'+data.text1+'</p><p id="txt2">'+data.text2+'</p></div><div id="btn"><button class="ctaBtn" style="line-height: 12px">'+data.ctaText+'</button></div></div></div>');

    $(document).ready(function(){
        $('#header_image').fadeIn(1000,function(){
            $('#txt1').fadeIn(1000,function(){
                $('#txt2').fadeIn(1000,function(){
                    $(".ctaBtn").fadeIn("slow");
                });
            });
        });
        $(".ctaBtn").click(function(){
            window.open(data.url,'_blank');  
        });
    });
	if (data.socialBlock === true || data.socialBlock === "true"){
		$('body').append('<div class="enable '+data.socialPos+'"><div class="fb-share-button fb-custom" data-href="'+data.fb+'" data-layout="button"></div><a href="https://twitter.com/intent/tweet?text='+data.tweet+'" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"/></div>');
	}
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
        if(name == undefined){
            name = null;
            return name;
        }
    unescape(url);
    results=url.split('?'+name+'=');
    if (!results) return null;
    if (!results[1]) return '';
    return unescape(results[1]);
}





        